Apresentação Fluig
// Após entendermos a estrutura de desenvolvimento, suas ferramentas e a estrutura dos projetos fluig. Vamos aprofundar mais nos aspectos técnicos da aplicação do fluig e seu desenvolvimento.
// Conforme tínhamos visto, o desenvolvimento sob o fluig trabalhada na camada front-end, evitando assim que o desenvolvedor necessite mexer no core da aplicação. Porém é fundamental entendermos o seu funcionamento e como as coisas funcionado do lado do server ou seja o back-end.
// O fluig por sua vez, foi desenvolvido sob a arquitetura da linguagem java (JEE), utilizando o JBoss (WildFly) como servidor de aplicação. Uma das vantagens da sua utilização do JavaEE é justamente o conjunto de especificações e bibliotecas voltadas as aplicações web, tais como Severlets e JSP.
// Quando construímos nossos projetos fluig, utilizamos o JavaScript como linguagem de desenvolvimento, por ser uma das linguagens do front-end, porém para o fluig todos os arquivos fontes .js são compilados em classes Java durante o processo de deployer. O rhino é a biblioteca responsável por fazer isso, ou seja podemos escrever fontes que manipulam o back-end com javascript por intermédio desta biblioteca, tornando imperceptível para o desenvolvedor a ação do rhino.


//pegar o iframe principal da pagina
$('iframe')[0]

//Busca campo de um Form
getCardValue("nome do form")

// Campos do tipo checkbox retornarem os valores on para marcado ou "" (vazio) para não marcado. 
// Para trabalhar de uma maneira mais fácil, é possível transformá-los em booleanos.
//Exemplo:
var campoCheckbox = hAPI.getCardValue("campoCheckbox") == "on" ? true : false;

//Retorna a empresa que o usuario esta ligado
WCMAPI.tenantCode ou WCMAPI.getTenantCode()

//deixar visivel por id , passa o id do form e ja eras
form.setVisibleById("dataContencao", true);


// exemplo de uso de dataset 

var usersByGroup = [];
		var c1 = DatasetFactory.createConstraint("colleagueGroupPK.groupId", grupoSelecionado, grupoSelecionado, ConstraintType.MUST);
    	var dsColleagueGroup = DatasetFactory.getDataset("colleagueGroup", null, new Array(c1), null);
    	for(var i = 0; i < dsColleagueGroup.values.length; i++){
    		usersByGroup.push(dsColleagueGroup.values[i]['colleagueGroupPK.colleagueId'])
    	}
    	console.log(usersByGroup)
		var dsColleague = DatasetFactory.getDataset("colleague", null, null, null);
		
//exemplo de opção selecionada e desabilitada
<option disabled selected value>Selecione uma prioridade</option>

// pegar o Iframe amaldiçoado
<body id="corpoChamado" onload="window.parent.document.getElementById('workflowView-cardViewer').style.border='none';"></body>

// pega um valor de um input de um formulario
hAPI.getCardValue("campo que deseja pegar");



//Buscando a ação do botão padrao de enviar do fluig com parent
$("#finalizar").on('click',function(){
	parent.$("[data-send]")[2].click();
});


//Exemplo de como pegar um elemento e fazer sumir aos poucos
$("#target3").addClass("animated fadeOut");

//Exemplo de como desabilitar um elemento
$("button").prop("disabled", true);


//Trocar um elemento de um lugar para outro
$("#target2").appendTo("#right-well");

//function chaining, copia um elemento e coloca em outro lugar
$("#target2").clone().appendTo("#right-well");

//Traz um elemento pai , junto com chaining css trocando de cor
$("#target1").parent().css("background-color", "red")

//Traz um elemento filho , junto com chaining css trocando de cor
$("#target1").children().css("background-color", "red")

//Como acessar nos filhos com a classe target
$(".target:nth-child(2)").addClass("animated bounce")

//Como acessar nos filhos com a classe target
$(".target:nth-child(2)").addClass("animated bounce")

//Como acessar os elementos impares ou pares
$(".target:odd").addClass("animated shake");
$(".target:even").addClass("animated shake");

//Como fazer o corpo do html inteiro sumir
$("body").addClass("animated fadeOut");

//Como fazer uma animação muito louca
$("body").addClass("animated hinge");

** DATASETS **

//consulta simples do dataset
var documentacao = DatasetFactory.getDataset("documentacao", null, null, null);
//vai trazer a quantidade de linhas
documentacao.values
//vai trazer o valor da linha , na coluna descrita no segundo array
documentacao.values[4]['dev'] //"Sérgio Ramos"


//FadeOut e depois de um tempo apaga da tela
$("#ocorrencia_reinc").fadeOut(800);							
setTimeout(function() {ocoRein.css("display","none")}, 1000);

//Fade Inline na index
<input type="radio" name="fgTipoSolicitacao" id="fgTipoSolicElab" value="fgTipoSolicElab" onclick="$('#div_revisar').fadeOut(600);">i18n.translate("tp_elaborar")<br>
<input type="radio" name="fgTipoSolicitacao" id="fgTipoSolicRevis" value="fgTipoSolicRevis" onclick="$('#div_revisar').fadeIn(600);">i18n.translate("tp_revisar")


 {/* Validação no Pai x Filho 
	percorrer , iterar, loop no pai x filho
*/}
        var indexes = form.getChildrenIndexes("ativPlano");
        if (indexes.length > 0) {
            for (var i = 0; i < indexes.length; i++) { // percorre os campos Pai x Filho
                if(form.getValue('nmResponsavelAtiv___' + indexes[i]) == null || form.getValue('nmResponsavelAtiv___' + indexes[i]) == '') {
                    throw "Informe o valor do campo de Responsável !";
                }
                if(form.getValue('nmResponsavelAtiv___' + indexes[i]) == form.getValue('nmResponsavelAtiv___' + indexes[i+1])) {
                    throw "Nome do campo não pode ser igual !";
                }
            }
         

















** Soluções para problemas day-by-day **
//Bloquear campos no Pai e Filho

$("[id^=campo_descricao___]").each(function(i ,v){
    $(this).attr("readonly", "readonly");//BLOQUEIA O CAMPO PARA EDIÇÃO
    $(this).parents("tr").find("[icone lixeira]").hide();//ou .remove() //ESCODE O ICONE DE DELETAR LINHA
});

// Ou

//Pega os Filhos do elemento do parâmetro e armazena numa variável
var indexes = form.getChildrenIndexes("tbAgendaViagem");
//Faz um loop para verificar o length
for (var i = 0; i < indexes.length; i++) {
//Desabilita o campo
form.setEnabled("atividade_"+ indexes[i], false);
form.setEnabled("calendarPeriodoDe_"+ indexes[i], false);
form.setEnabled("calendarPeriodoAte_"+ indexes[i], false);
}


//Adicionar o calendar dentro de um pai x filho 

/*
 Criar a função e chamar inline em um button fora da table
*/
function addFilhoTabelha() {
	var add= wdkAddChild('ativPlano');
    //Toda vez que criar um filho, ele coloca o calendário no campo
    FLUIGC.calendar("#nmDtInicial___"+add);
  }


  //pega o id e a matricula do usuario selecionado no input Responsavel
  //não precisa entender , é só usar....
function setSelectedZoomItem(data) {
	var inputId = data.inputId.split('___');
	var row = inputId[1];
	$('#nmResponsavelAtiv___' + row).val(data.colleagueName);
	$('#idResponsavelAtiv___' + row).val(data.colleagueId);
}



//Pega uma data inicio e uma data fim do formulario , divide em 3 numeros dia
//mes e ano , compara e faz uma verificacao pra data fim nao ser menor que a data inicio
var dataInicio = form.getValue("dtDataInicioAudit").split('/');
		var dataFim = form.getValue("dtDataInvertFimAudit").split('/');
		
		if ( dataInicio[2] > dataFim[2] )
			throw "A data fim não pode ser menor que a data de início.";
		
		else if ( dataInicio[1] > dataFim[1] )
			throw "A data fim não pode ser menor que a data de início.";
			
		else if ( dataInicio[0] > dataFim[0] )
			throw "A data fim não pode ser menor que a data de início.";


//achei legal
var beforeSendValidate = function(numState, nextState) {

    if (numState == 2) { // codigo da atividade
        if ($("table[tablename='suaTableName'] tbody tr").length > 1) {
                $("input[id^='algumCampodaTable___']").each(function(index) {
                    var id = $(this).attr("id").split("___")[1];

                    if ($("#algumCampodaTable___" + id).val() == "")
                        throw "Campo obrigatório"
                });
        }
    }
}


//Validação da data fim nao pode ser menor que o inicio
var dataInicio = form.getValue("dtDataInicioAudit").split('/');
var dataFim = form.getValue("dtDataInvertFimAudit").split('/');

if ( dataInicio[2] > dataFim[2] )
    msg+= "A data fim não pode ser menor que a data de início.\n";

else if ( dataInicio[1] > dataFim[1] )
    msg+= "A data fim não pode ser menor que a data de início.\n";
    
else if ( dataInicio[0] > dataFim[0] )
    msg+= "A data fim não pode ser menor que a data de início.\n";
    

//Criar Botão no form para pegar o envio de evento
    $("#id_do_campo").click(function(){
        $("#workflowActions > button:first-child", window.parent.document).click(); 
  });

 
//populando os filhos do pai x filho
  function beforeStateEntry(sequenceId) {
    if (sequenceId == 4) {
        var childData = new java.util.HashMap();
        childData.put("matricula", "0041");
        childData.put("nome", "João Silva");
        childData.put("cpf", "44455889987");
        hAPI.addCardChild("funcionarios", childData);
    }
}

// Exemplo de como pegar os valores do campo do form e jogar nos document
function afterTaskSave(colleagueId,nextSequenceId,userList){
	
	var arr = [];
	
	var usuario = getValue('WKUser'); //usuário logado
	var numSolicitacao = getValue("WKNumProces");
	var mensagem = hAPI.getCardValue('tituloAssunto'); //aqui, no lugar de 'problema', insira o nome do seu campo
	var mensagem2 = hAPI.getCardValue('dsocorrencia'); //aqui, no lugar de 'problema', insira o nome do seu campo
	var cardData = hAPI.getCardData(numSolicitacao);
	//arr.push(mensagem2,mensagem);
	
	var keys = cardData.keySet().toArray();
	
	for (var key in keys) {
		var field = keys[key];
		log.info('lalala ' + field); // campo do formulario
		log.info('sadiademon ' + cardData.get('nmArea')); // campo do formulario

		//pega o valor do campo 
		if(cardData.get(field) !== '' || cardData.get(field) !== null || cardData.get(field) !== undefined) {
			arr.push(cardData.get(field));
		} 
	}